<?php

/**
 * Used to connect to any Database (MySQL, Oracle, MSSQL).
 * 
 */
interface IConnection {

    public function connect($driver, $name, $host, $db, $user, $pass, $charset, $options);
    public function disconnect();

}