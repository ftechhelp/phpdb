<?php
require __DIR__ . "/IConnection.php";

class SQLConnection implements IConnection {

    private $pdo;

    /**
     * Connects to SQL database.
     * 
     * Return Type: void
     */
    public function connect ($driver, $name, $host, $db, $user, $pass, $charset, $options = "default"){

        $dsn = "$driver:host=$host;dbname=$db;charset=$charset";

        $options = $options != "default" ? $options : [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES   => false,
        ];

        try {

            $this->pdo = new PDO($dsn, $user, $pass, $options);

            print("Connected successfully to $name. \r\n");

        }catch (PDOException $e){

            throw new PDOException($e->getMessage(), (int)$e->getCode());

        }

    }

    /**
     * Disconnects from SQL database.
     * 
     * Return Type: void
     */
    function disconnect (){

        $this->pdo = null;

    }

    /**
     * Helper to query the datab.
     * 
     * Return Type: QueryDatabase
     */
    function query(){

        return new QueryDatabase();

    }
}