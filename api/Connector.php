<?php
require __DIR__ . "/Connections/SQLConnection.php";


class Connector {

    private $databaseConnections = [];
    private $databaseConnectionInformation = [];

    function __construct(){

        $this->databaseConnectionInformation = $this->getConnections();

    }

    /**
     * Connects to database that matches the given ID.
     * 
     * Return Type: SQLConnection
     */
    public function connect($id){
        
        //make sure ID exists
        if ($this->databaseConnectionInformation[$id]["host"] == null){

            print("Could not connect to the database because the ID given does not exist. \r\n");
            return;

        }

        //make sure connection is not already made
        if (key_exists($id, $this->databaseConnections)){

            print("Database '" . $this->databaseConnections[$id]["name"] . "' is already connected. \r\n");
            return;

        }

        $databaseConnection = new SQLConnection();

        if ($databaseConnection->connect(
            $this->databaseConnectionInformation[$id]["type"],
            $this->databaseConnectionInformation[$id]["name"], 
            $this->databaseConnectionInformation[$id]["host"], 
            $this->databaseConnectionInformation[$id]["db"], 
            $this->databaseConnectionInformation[$id]["user"], 
            $this->databaseConnectionInformation[$id]["pass"], 
            $this->databaseConnectionInformation[$id]["charset"]
        )){

            $this->databaseConnections[$id] = $databaseConnection;

        }

    }

    /**
     * Connects to every database in database_connections.json.
     * If isArchived is set to 1, it will not connect to that database.
     */
    public function connectAll(){

        foreach ($this->databaseConnectionInformation as $key => $connection){

            if ($connection["isArchived"] == 1)
                continue;
            
            $this->connect($key);

        }

    }

    /**
     * 
     * 
     */

    /**
     * Returns an list of connection information.
     * 
     * Return type: Array
     */
    private function getConnections(){

        $databaseConnectionsString = file_get_contents(__DIR__ . "/Connections/database_connections.json", true);

        return json_decode($databaseConnectionsString, true);

    }

}