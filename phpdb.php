<?php
require "./api/Connector.php";

class PHPDB {

    public function load($id){

        $connector = new Connector();

        return $connector->connect($id);

    }

    public function loadAll(){

        $connector = new Connector();

        $connector->connectAll();

    }

    public function unload($id){



    }

    public function unloadAll(){



    }

}